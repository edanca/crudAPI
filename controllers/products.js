const fs = require('fs')
const fileName = 'db.json'

// Destructuren
const { products } = require('../db.json')
// al pasarle solo la variable,
// se le esta diciendo que la llave tiene el mismo
// nombre que el valor


function writeFile(products) {
	fs.writeFile(fileName, JSON.stringify({products}), (err) => {
		if (err) throw err
		console.log('Saved!')
	})
	return true
}

module.exports = {

    getProducts: (req, res) => {
        console.log(JSON.stringify(products))
        res.json({products})
    },


    addProduct: (req, res) => {
        // const product = req.body.name
        const { name } = req.body
        products.push({
            name,
            id: products.length
        })

		writeFile(products)

        res.json({
            "success": true,
			"msg": "successfull",
			"productAdded": products[products.length-1]
        })
    },


    updateProduct: (req, res) => {
        const { id } = req.params
        const { name } = req.body

        products.forEach((product, i) => {
            if (product.id === Number(id)) {
                product.name = name
            }
        })

        writeFile(products)

        res.json({
            "sucess": true,
			"msg": "updated",
			"productUpdated": products[id]
        })
    },


    deleteProduct: (req, res) => {
        const { id } = req.params
        products.forEach((product, i) => {
            if (product.id === Number(id)) {
                products.splice(i, 1)
            }
		})
		
		writeFile(products)

        res.json({
            "sucess": true,
            "msg": 'deleted'
        })
    }
}