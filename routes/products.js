const express = require('express')
const router = express.Router()

// const controllers = require('../controllers/products')
const {
    getProducts,
    addProduct,
    updateProduct,
    deleteProduct
} = require('../controllers/products')

router.route('/')
    .get(getProducts)
    .post(addProduct)


// localhost:3000/product/
router.route('/:id')
    .put(updateProduct)
    .delete(deleteProduct)


// Exportar el modulo
module.exports = router
