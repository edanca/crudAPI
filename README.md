# CRUD API

This API was build using:
* Express
* Body-parser
* Morgan

## Funcionality
> To return the elements availables from a **JSON** file through an API.

## How to use it
First, you need to install the tools listed above.

Second, you need to up the server.

Third, you need to send the commnads through the routes.

## Routes

```
http://127.0.0.1:3000/products
http://127.0.0.1:3000/
http://127.0.0.1:3000/
```
