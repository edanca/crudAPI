const morgan = require('morgan') // HTTP request library
const bodyParser = require('body-parser')

const express = require('express')
const app = express()

const productsRoutes = require('./routes/products')

// Settings
app.set('json spaces', 4)


// Middlewares
app.use(morgan('dev'))
app.use(bodyParser.json()) // entender formato json
app.use(bodyParser.urlencoded({extended: false})) //entender datos de un formulario

// Routes
app.use('/products', productsRoutes)


// Static Files


// Start Server
app.listen(3000, () => {
    console.log('Server on Port' , 3000)
})
